

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.ActivityGroup;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.res.ColorStateList;
import android.content.res.XmlResourceParser;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import android.util.SparseIntArray;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;



/**
 * 工具类
 */
public class Toolkit {

    private static final String TAG = "Toolkit";

    /**
     * 隐藏状态栏和标题栏
     * @see [类、类#方法、类#成员]
     * @since [产品/模块版本]
     */
    public static void hideStatusBar(Activity activity) {
        activity.requestWindowFeature(Window.FEATURE_NO_TITLE);
        int flag = WindowManager.LayoutParams.FLAG_FULLSCREEN;
        activity.getWindow().setFlags(flag, flag);
    }

    /**
     * 下载网络图片
     * @param context
     * @param id
     * @return
     * @see [类、类#方法、类#成员]
     * @since [产品/模块版本]
     */
    public static Bitmap getBitmap(String url) {
        URL myFileUrl = null;
        Bitmap bitmap = null;
        try {
            myFileUrl = new URL(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        try {
            HttpURLConnection conn = (HttpURLConnection) myFileUrl.openConnection();
            conn.setDoInput(true);
            conn.connect();
            InputStream is = conn.getInputStream();
            bitmap = BitmapFactory.decodeStream(is);
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    /**
     * 从图片服务器获取指定文件名的图片
     * @param serverPath 图片服务器HTTP路径
     * @param imageName 图片文件名
     * @return
     * @see [类、类#方法、类#成员]
     * @since [产品/模块版本]
     */
    public static Bitmap getBitmap(String serverPath, String imageName) {
        return getBitmap(serverPath + imageName);
    }

    /**
     * 从默认图片服务器获取指定文件名的图片
     * @param imageName
     * @return
     * @see [类、类#方法、类#成员]
     * @since [产品/模块版本]
     */
    public static Bitmap getBitmapByName(String imageName) {
        return getBitmap(EPGData.imageBasePath, imageName);
    }

    /**
     * 将日期转换成字符串
     * @param date 待转换日期
     * @param pattern 转换格式模板
     * @return 格式化转换后的字符串
     */
    public static String dateToString(Date date, String pattern) {
        if (date == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        return sdf.format(date);
    }

    /**
     * 将字符串转换成日期，默认格式：yyyy-MM-dd
     * @param string - 日期字符串
     * @return 按默认格式转换后的日期
     */
    public static Date stringToDate(String string) {
        if (Toolkit.isEmptyString(string)) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return sdf.parse(string);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 将从网关获取的yyyyMMddHHmmss时间字符串转换为yyyy-MM-dd HH:mm:ss
     * @return
     * @see [类、类#方法、类#成员]
     * @since [产品/模块版本]
     */
    public static String formatNormalDate(String dateStr) {
        if (Toolkit.isEmptyString(dateStr)) {
            return null;
        }
        SimpleDateFormat originalSdf = new SimpleDateFormat("yyyyMMddHHmmss");
        SimpleDateFormat normalSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = originalSdf.parse(dateStr);
            return normalSdf.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 将从网关获取的yyyyMMddHHmmss时间字符串转换为yyyy.MM.dd
     * @return
     * @see [类、类#方法、类#成员]
     * @since [产品/模块版本]
     */
    public static String formatOriginalDate(String dateStr) {
        if (Toolkit.isEmptyString(dateStr)) {
            return null;
        }
        SimpleDateFormat originalSdf = new SimpleDateFormat("yyyyMMddHHmmss");
        SimpleDateFormat normalSdf = new SimpleDateFormat("yyyy.MM.dd");
        Date date = null;
        try {
            date = originalSdf.parse(dateStr);
            return normalSdf.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 将从网关获取的yyyyMMddHHmmss时间字符串转换为yyyy-MM-dd
     * @return
     * @see [类、类#方法、类#成员]
     * @since [产品/模块版本]
     */
    public static String formatDate(String dateStr) {
        if (Toolkit.isEmptyString(dateStr)) {
            return null;
        }
        SimpleDateFormat originalSdf = new SimpleDateFormat("yyyyMMddHHmmss");
        SimpleDateFormat normalSdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = originalSdf.parse(dateStr);
            return normalSdf.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 将书签播放信息的播放位置或者影片时间(以秒为单位)格式化为HH:mm:ss
     * @param position
     * @return
     * @see [类、类#方法、类#成员]
     * @since [产品/模块版本]
     */
    public static String formatTimeStr(int position) {
        int secs = position % 60;
        int mins = position / 60;
        if (mins >= 60) {
            int hours = mins / 60;
            mins = mins % 60;
            return ((hours >= 10) ? (hours + "") : ("0" + hours)) + ":"
                    + ((mins >= 10) ? (mins + "") : ("0" + mins)) + ":"
                    + ((secs >= 10) ? (secs + "") : ("0" + secs));
        } else {
            return ((mins >= 10) ? (mins + "") : ("0" + mins)) + ":"
                    + ((secs >= 10) ? (secs + "") : ("0" + secs));
        }
    }

    /**
     * 把期集(例如：20120202)转换为yyyy-MM-dd
     * @param expect
     * @return
     */
    public static String formatExpectTime(String expect) {
        if (Toolkit.isEmptyString(expect) || !Toolkit.isInteger(expect)) {
            return "";
        }
        String expectTime = expect.trim();
        if (expectTime.length() < 8) {
            return "";
        }
        //截取年月日
        String year = expectTime.substring(0, 4);
        String month = expectTime.substring(4, 6);
        String day = expectTime.substring(6, 8);
        return year + "-" + month + "-" + day;
    }

    /**
     * 把网关返回的时间（秒）转换成天
     * @param time
     * @return
     */
    public static int formatTime(int time) {
        if (time <= 0) {
            return 0;
        } else {
            int oneDay = 3600 * 24;
            if (time % oneDay == 0) {
                return time / oneDay;

            } else {
                return (time / oneDay) + 1;
            }

        }

    }

    /**
     * 改变时间格式
     * @param timeStr
     */
    public static String changeTimeFormat(String timeStr) {
        if (Toolkit.isEmptyString(timeStr)) {
            return "";
        }
        SimpleDateFormat originalSdf = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat normalSdf = new SimpleDateFormat("HH:mm");
        Date date = null;
        try {
            date = originalSdf.parse(timeStr);
            return normalSdf.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * 获得星期几
     * @param dateStr yyyy-MM-dd
     * @return
     */
    public static String getWeekByDate(String dateStr) {
        SimpleDateFormat originalSdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            originalSdf.setTimeZone(TimeZone.getTimeZone("GMT+8"));
            date = originalSdf.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        cal.setTime(date);
        int i = cal.get(Calendar.DAY_OF_WEEK);
        String[] dayNames = {"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};
        return dayNames[i - 1];
    }

    /**
     * 判断是否能播放，如果当前系统时间比节目可播时间点大，则表示节目可以播放
     * @param systime 服务器时间，距离1970-1-1 00:00:00的秒数
     * @param time 节目可播时间点 yyyyMMddHHmmss
     * @return
     */
    public static boolean isCanPlay(int systime, String time) {
        SimpleDateFormat originalSdf = new SimpleDateFormat("yyyyMMddHHmmss");
        Date date = null;
        try {
            originalSdf.setTimeZone(TimeZone.getTimeZone("GMT+8"));
            date = originalSdf.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
        Calendar mCalendar = Calendar.getInstance();
        mCalendar.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        mCalendar.setTimeInMillis(systime * 1000L);
        Date sysDate = mCalendar.getTime();

        if (sysDate.compareTo(date) > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 判断传入参数是否为空,空字符串""或"null"
     * @param s 待判断参数
     * @return true 空 <br>
     *         false 非空
     */
    public static boolean isEmptyString(Object s) {
        return (s == null) || (s.toString().trim().length() == 0)
                || s.toString().trim().equalsIgnoreCase("null");
    }

    /**
     * Uppercase first letter.
     * @param string
     * @return
     */
    public static String toUpperCaseFirstLetter(String string) {
        if (string == null) {
            return string;
        }
        StringBuilder builder = new StringBuilder(string.trim());
        if (builder.length() > 0) {
            builder.replace(0, 1, string.substring(0, 1).toUpperCase());
        }
        return builder.toString();
    }

    /**
     * Lowercase first letter.
     * @param string
     * @return
     */
    public static String toLowerCaseFirstLetter(String string) {
        if (string == null) {
            return string;
        }
        StringBuilder builder = new StringBuilder(string.trim());
        if (builder.length() > 0) {
            builder.replace(0, 1, string.substring(0, 1).toLowerCase());
        }
        return builder.toString();
    }

    public static int ceil(int a, int b) {
        return Math.round((float) Math.ceil((double) a / b));
    }

    /**
     * 退出应用程序
     * @param context
     * @see [类、类#方法、类#成员]
     * @since [产品/模块版本]
     */
    public static void exitApp(Context context) {
        Logger.getLogger().d("app exit");


        if (context instanceof ActivityGroup) {
            ((ActivityGroup) context).getLocalActivityManager().removeAllActivities();
        }
        if (context instanceof Activity) {
            Activity activity = (Activity) context;
            activity.finish();
        }
        MyActivityManager.getInstance().popAllActivity();
        System.gc();
        if (!Product.isSupportCoreService()) {
            //销毁缓冲代理
            //FIXME
            //            if (Product.isSupportBuffGateway()) {
            //                try {
            //                    BuffGatewayHelper helper = BuffGatewayHelper.getInstance();
            //                    if (helper.isInit()) {
            //                        helper.destroy();
            //                    }
            //                } catch (Exception e) {
            //                    e.printStackTrace();
            //                }
            //            }
            //销毁网关
            Logger.getLogger().d("NMSProxy.Destory");
            try {
                NMSProxyHelper helper = NMSProxyHelper.newInstance();
                if (helper.isInitComplete()) {
                    helper.destroy();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //        android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(0);
        }
    }

    /**
     * 执行unix命令
     * @param command
     * @return
     * @see [类、类#方法、类#成员]
     * @since [产品/模块版本]
     */
    public static String exec(String command) {
        try {
            Process process = Runtime.getRuntime().exec(command);
            //            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            //            int read;
            //            char[] buffer = new char[4096];
            //            StringBuffer output = new StringBuffer();
            //            while ((read = reader.read(buffer)) > 0) {
            //                output.append(buffer, 0, read);
            //            }
            //            reader.close();
            //            process.waitFor();
            //            return output.toString();
            return null;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 将apk中的资源文件下载到系统文件目录中
     * @param name
     * @param localPath
     * @see [类、类#方法、类#成员]
     * @since [产品/模块版本]
     */
    public static void download(Context context, String name, String localPath) {
        try {
            InputStream in = context.getResources().getAssets().open(name);
            File outFile = new File(localPath);
            FileOutputStream out = new FileOutputStream(outFile);
            int read;
            byte[] buffer = new byte[4096];
            while ((read = in.read(buffer)) > 0) {
                out.write(buffer, 0, read);
            }
            out.close();
            in.close();
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 如果焦点从GridView上的一个Item移出，使GridView失去焦点，当焦点返回GridView时，
     * 如果第一个获得焦点的Item是上次最后移出那个Item此时不会触发OnItemSelectedListener中的onItemSelected事件，
     * 使用下面反射的代码清除默认焦点，达到预期效果
     * @param gridView
     */
    public static void clearGridViewFocus(GridView gridView) {
        try {
            @SuppressWarnings("unchecked")
            Class<GridView> c = (Class<GridView>) Class.forName("android.widget.GridView");
            Method[] flds = c.getDeclaredMethods();
            for (Method f: flds) {
                if ("setSelectionInt".equals(f.getName())) {
                    f.setAccessible(true);
                    f.invoke(gridView, new Object[] {Integer.valueOf(-1)});
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setGridViewSelection(GridView gridView, int position) {
        try {
            @SuppressWarnings("unchecked")
            Class<GridView> c = (Class<GridView>) Class.forName("android.widget.GridView");
            Method[] flds = c.getDeclaredMethods();
            for (Method f: flds) {
                if ("setSelectionInt".equals(f.getName())) {
                    f.setAccessible(true);
                    f.invoke(gridView, new Object[] {Integer.valueOf(position)});
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 如果焦点从GridView上的一个Item移出，使GridView失去焦点，当焦点返回GridView时，
     * 如果第一个获得焦点的Item是上次最后移出那个Item此时不会触发OnItemSelectedListener中的onItemSelected事件，
     * 使用下面反射的代码清除默认焦点，达到预期效果
     * @param listView
     */
    public static void clearListViewFocus(ListView listView) {
        try {
            @SuppressWarnings("unchecked")
            Class<ListView> c = (Class<ListView>) Class.forName("android.widget.ListView");
            Method[] flds = c.getDeclaredMethods();
            for (Method f: flds) {
                if ("setSelectionInt".equals(f.getName())) {
                    f.setAccessible(true);
                    f.invoke(listView, new Object[] {Integer.valueOf(-1)});
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 去掉空格
     * @param str
     * @return
     * @see [类、类#方法、类#成员]
     * @since [产品/模块版本]
     */
    public static String deleteSpace(String str) {
        if ((str == null) || (str.trim().length() == 0)) {
            return null;
        }
        String[] string = str.split(",");
        StringBuffer sb = new StringBuffer();
        String newStr = "";
        for (String s: string) {
            sb.append(s.trim());
            sb.append(",");
        }
        if (sb.length() > 1) {
            newStr = sb.substring(0, sb.length() - 1);
        }
        return newStr;
    }

    /**
     * 判断字符串是否是整型
     * @param str
     * @return
     * @see [类、类#方法、类#成员]
     * @since [产品/模块版本]
     */
    public static boolean isInteger(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 判断字符串是否为null或空
     * @param str
     * @return
     * @see [类、类#方法、类#成员]
     * @since [产品/模块版本]
     */
    public static boolean isEmpty(String str) {
        return ((str == null) || (str.length() == 0));
    }

    /**
     * 判断稀疏数组是否为null或空
     * @param arr
     * @return
     */
    public static <E> boolean isEmpty(SparseArray<E> arr) {
        return ((arr == null) || (arr.size() == 0));
    }

    /**
     * 判断稀疏数组是否为null或空
     * @param arr
     * @return
     */
    public static boolean isEmpty(SparseIntArray arr) {
        return ((arr == null) || (arr.size() == 0));
    }

    /**
     * 判断稀疏数组是否为null或空
     * @param arr
     * @return
     */
    public static boolean isEmpty(SparseBooleanArray arr) {
        return ((arr == null) || (arr.size() == 0));
    }

    /**
     * 判断集合是否为null或空
     * @param collection
     * @return
     * @see [类、类#方法、类#成员]
     * @since [产品/模块版本]
     */
    @SuppressWarnings("rawtypes")
    public static boolean isEmpty(Collection collection) {
        return ((collection == null) || (collection.isEmpty()));
    }

    /**
     * 判断Map是否为null或空
     * @param map
     * @return
     * @see [类、类#方法、类#成员]
     * @since [产品/模块版本]
     */
    @SuppressWarnings("rawtypes")
    public static boolean isEmpty(Map map) {
        return ((map == null) || (map.isEmpty()));
    }

    /**
     * 将字符串中所有的字符全角化，即将所有的数字、字母及标点全部转为全角字符，使它们与汉字同占两个字节
     * @param input
     * @return
     */
    public static String ToDBC(String input) {
        if (isEmpty(input)) {
            return "";
        }
        char[] c = input.toCharArray();
        for (int i = 0; i < c.length; i++) {
            if (c[i] == 12288) {
                c[i] = (char) 32;
                continue;
            }
            if ((c[i] > 65280) && (c[i] < 65375)) {
                c[i] = (char) (c[i] - 65248);
            }
        }
        return new String(c);
    }

    /**
     * 去掉字符串中的标点符号和空格
     * @param input
     * @return
     */
    public static String deleteSpaceAndPunct(String input) {
        if (isEmpty(input)) {
            return "";
        }
        return input.replaceAll("[\\p{Punct}\\p{Space}]+", "");
    }

    /**
     * 分割字符串,去除了分割出每项中的空字符（"" , " "）
     * @param source 字符串
     * @param div 分割标识
     * @param isDeleted 是否去除分割出每项中的标点符号和空格
     * @return
     */
    public static String[] splitString(String source, String div, boolean isDeleted) {
        if (source == null || source.trim().equals("")) {
            return null;
        }
        String[] arr = source.split("\\" + div);
        StringBuffer sb = new StringBuffer();
        for (String s: arr) {
            if (!s.trim().equals("")) {
                if (isDeleted) {
                    s = deleteSpaceAndPunct(s);
                }
                sb.append(s + div);
            }
        }
        String[] strValue = sb.substring(0, sb.length() - 1).split("\\" + div);
        return strValue;

    }

    /**
     * 让线程睡眠一段时间
     * @param times
     */
    public static void sleep(long times) {
        try {
            Thread.sleep(times);
        } catch (InterruptedException e) {
            Logger.getLogger().e(e.getMessage());
        }
    }

    /**
     * 判断指定的服务是否启动
     * @param context 系统上下文
     * @param serviceName 服务名称为包名+类名
     * @return
     */
    public static boolean isServiceWorked(Context context, final String serviceName) {
        ActivityManager activityManager = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> serviceList = activityManager
                .getRunningServices(30);
        //要判断的服务名字
        for (int i = 0; i < serviceList.size(); i++) {
            if (serviceName.equals(serviceList.get(i).service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断系统是否存在该应用
     * @param context
     * @param packName
     * @return
     */
    public static boolean isExistApp(Context context, String packName) {
        List<PackageInfo> packages = context.getPackageManager().getInstalledPackages(0);
        for (int i = 0; i < packages.size(); i++) {
            PackageInfo packageInfo = packages.get(i);
            if (packageInfo.packageName.equals(packName)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 获取本地版本字符串(AndroidManifest.xml中配置的版本信息)
     * @return
     */
    public static String getLocalVersion(Context context) {
        PackageManager pm = context.getPackageManager();
        String version = null;
        try {
            PackageInfo pi = pm.getPackageInfo(context.getPackageName(), 0);
            version = pi.versionName;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
        return version;
    }

    /**
     * 检测网络是否可用
     * @param ctx
     * @return
     */
    public static boolean isNetworkAvailable(Context ctx) {
        //TODO 4.0 虚拟机不进行网络检测
        if (Build.PRODUCT.equalsIgnoreCase("eeepc") && Build.CPU_ABI.equalsIgnoreCase("x86")) {
            return true;
        }

        ConnectivityManager cm = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm == null) {
            return false;
        }
        NetworkInfo[] networkInfos = cm.getAllNetworkInfo();
        for (NetworkInfo network: networkInfos) {
            Logger.getLogger().d(
                    network.getTypeName() + "-->" + "state:" + network.getState()
                            + ", isConnected:" + network.isConnected() + ", isAvailable:"
                            + network.isAvailable());
            if (network.isConnected()) {
                //如果是Hi3716且连接是wifi的话需要检查wifi的具体信息，因为设备有问题
                //Hi3716真正的wifi网络名为wifi,type为1，有线网络名为WIFI,type也为1。
                if (Product.isHuaWei_Hi3716() && network.getType() == ConnectivityManager.TYPE_WIFI) {
                    WifiManager wifiManager = (WifiManager) ctx
                            .getSystemService(Context.WIFI_SERVICE);
                    if (wifiManager != null) {
                        if (wifiManager.getWifiState() == WifiManager.WIFI_STATE_ENABLED) {
                            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                            if (wifiInfo != null) {
                                Logger.getLogger().d(
                                        "wifiInfo:" + wifiInfo + ", rssi:" + wifiInfo.getRssi()
                                                + ", ssid:" + wifiInfo.getSSID());
                                String ssid = wifiInfo.getSSID();
                                SupplicantState state = wifiInfo.getSupplicantState();
                                if (ssid != null && state == SupplicantState.COMPLETED) {
                                    return true;
                                }
                            }
                        }
                    }
                    return false;
                } else {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 检测wifi是否连接上了
     * @param ctx
     * @return
     */
    public static boolean isWifiConnected(Context ctx) {
        ConnectivityManager cm = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = (cm != null) ? cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
                : null;
        return (networkInfo != null) ? networkInfo.isConnected() : false;
    }

    /**
     * 删除全部文件
     * @param paramFile
     */
    public static void deleteFile(File paramFile) {
        if (paramFile.exists()) {
            if (!paramFile.isFile()) {
                if (paramFile.isDirectory()) {
                    File[] arrayOfFile = paramFile.listFiles();
                    for (int i = 0; i < arrayOfFile.length; i++) {
                        deleteFile(arrayOfFile[i]);
                    }
                }
            } else {
                paramFile.delete();
            }
            paramFile.delete();
        }
    }

    /**
     * 禁用ScrollView、ListView等控件拖动到边缘时显示黄光的效果
     * <p>
     * 因{@link android.view.View#setOverScrollMode(int)}是从API9(2.3)开始才有的，所以API9以下的使用反射来调用
     * @param view
     */
    @SuppressWarnings({"rawtypes"})
    public static void disableOverScrollMode(View view) {
        try {
            Class viewClass = View.class;
            Method setOverScrollMode = viewClass.getMethod("setOverScrollMode", int.class);
            //View.OVER_SCROLL_NEVER = 2
            setOverScrollMode.invoke(view, 2);
        } catch (Exception e) {
            //            e.printStackTrace();
        }
    }

    /**
     * 设置文字颜色
     * @param vew
     */
    public static void createTextColor(Context context, TextView vew, int resId) {
        XmlResourceParser xml = context.getResources().getXml(resId);
        ColorStateList colorStateList;
        try {
            colorStateList = ColorStateList.createFromXml(context.getResources(), xml);
            vew.setTextColor(colorStateList);
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 将一个媒资列表转换为以媒资Id为key、媒资对象为value的map
     * @param mediaInfos
     * @return
     */
    public static SparseArray<MediaInfo> convertList2Map(List<MediaInfo> mediaInfos) {
        if (isEmpty(mediaInfos)) {
            return null;
        }
        SparseArray<MediaInfo> mediaInfoArray = new SparseArray<MediaInfo>();
        for (MediaInfo mediaInfo: mediaInfos) {
            String mediaId = mediaInfo.getId();
            String ref = mediaInfo.getRef();
            if (!Toolkit.isEmpty(mediaId) && Toolkit.isInteger(mediaId)) {
                mediaInfoArray.put(Integer.parseInt(mediaId), mediaInfo);
            } else if (!Toolkit.isEmpty(ref) && Toolkit.isInteger(ref)) {
                mediaInfoArray.put(Integer.parseInt(ref), mediaInfo);
            }
        }
        return mediaInfoArray;
    }

    /**
     * 判断当前应用是否是debuggable模式
     * @return
     */
    public static boolean isDebuggable() {
        return 0 != (Application.getInstance().getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE);
    }

    /**
     * 保存Logcat日志 <br/>
     * //TODO 还需完善，执行保存logcat日志的Process可能有多个
     * @param fileName
     */
    public static boolean saveLogToFile(String fileName) {
        boolean successed = false;
        Runtime runtime = Runtime.getRuntime();
        try {
            String str = String.format("logcat -d -v time -f %s\n", new Object[] {fileName});
            Process process = runtime.exec(str);
            process.waitFor();
            int exitValue = process.exitValue();
            Logger.getLogger().i("saveLogToFile finished,exitCode = " + exitValue);
            if (exitValue != 0) {
                successed = false;
            } else {
                successed = true;
            }

        } catch (Exception e) {
            Logger.getLogger().e("saveLogToFile failed", e);
            successed = false;
        }
        return successed;
    }

    /**
     * 清除LogCat日志
     */
    public static boolean clearLogCat() {
        boolean successed = false;
        Runtime runtime = Runtime.getRuntime();
        try {
            String str = "logcat -c";
            Process process = runtime.exec(str);
            process.waitFor();
            int exitValue = process.exitValue();
            Logger.getLogger().i("clearLogCat finished,exitCode = " + exitValue);
            if (exitValue != 0) {
                successed = false;
            } else {
                successed = true;
            }
        } catch (Exception e) {
            Logger.getLogger().e("clearLogCat failed", e);
            successed = false;
        }
        return successed;
    }

    /**
     * px转化为dip/dp
     * @param context
     * @param pxValue
     * @return
     */
    public static int px2dip(Context context, int pxValue) {
        final float density = context.getResources().getDisplayMetrics().density;
        return Math.round(density * pxValue);
    }

    /**
     * 安装Apk
     * @param context
     * @param file
     */
    public static void installApk(Context context, File file) {
        Intent intent = new Intent();
        //执行动作
        intent.setAction(Intent.ACTION_VIEW);
        //执行的数据类型
        intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
        context.startActivity(intent);
    }

    /**
     * 静默安装Apk
     * @param context
     * @param file
     */
    public static void installApkSilent(Context context, File file) {
        //要安装的apk包名
        String packageName = "org.zhan.test";
        try {
            Class packagemanage = Class.forName("android.content.pm.PackageManager");

            Class packageInstallObserver = Class
                    .forName("android.content.pm.IPackageInstallObserver");

            Method installPackage = packagemanage.getMethod("installPackage", Uri.class,
                    packageInstallObserver, int.class, String.class);
            //0x00000002
            int INSTALL_REPLACE_EXISTING = packagemanage.getField("INSTALL_REPLACE_EXISTING")
                    .getInt(null);
            Object iActivityManager = installPackage.invoke(context.getPackageManager(),
                    Uri.fromFile(file), null, INSTALL_REPLACE_EXISTING, packageName);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 启动网络设置
     * @param context
     */
    public static void startNetworkSettings(Context context) {
        Intent intent = null;

        //判断手机系统的版本  即API大于10 就是3.0或以上版本 
        if (Product.isTCL_MS801() || Product.isTCL_MT55()) {
            intent = context.getPackageManager()
                    .getLaunchIntentForPackage("com.tcl.mstar.settings");
            //            ComponentName component = new ComponentName("com.tcl.mstar.settings",
            //                    "com.tcl.mstar.settings.NetworkSettingsActivity");
            //            intent.setComponent(component);
        } else if (Product.isCH_MT5396()) {
            intent = context.getPackageManager().getLaunchIntentForPackage(
                    "com.changhong.dmt.system.setting");
        } else if (android.os.Build.VERSION.SDK_INT > 10) {
            intent = new Intent(android.provider.Settings.ACTION_SETTINGS);
        } else {
            intent = new Intent();
            ComponentName component = new ComponentName("com.android.settings",
                    "com.android.settings.Settings");
            intent.setComponent(component);
            intent.setAction("android.intent.action.VIEW");
        }
        context.startActivity(intent);
    }

    /**
     * 保存属性到SystemProperties中
     * @param key
     * @param value
     */
    public static void setprop(String key, String value) {
        try {
            Class clazz = Class.forName("android.os.SystemProperties");
            Method method = clazz.getMethod("set", String.class, String.class);
            Object obj = method.invoke(clazz, key, value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 从SystemProperties获取属性值
     * @param key
     * @return
     */
    public static String getprop(String key) {
        String override = null;
        try {
            Class clazz = Class.forName("android.os.SystemProperties");
            Method method = clazz.getMethod("get", String.class);
            Object obj = method.invoke(clazz, key);
            if (obj != null) {
                override = obj.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return override;
    }

    /**
     * 获得属于桌面的应用的应用包名称
     * @return 返回包含所有包名的字符串列表
     */
    private static List<String> getHomes() {
        List<String> names = new ArrayList<String>();
        PackageManager packageManager = Application.getInstance().getPackageManager();
        //属性
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        List<ResolveInfo> resolveInfo = packageManager.queryIntentActivities(intent,
                PackageManager.MATCH_DEFAULT_ONLY);
        for (ResolveInfo ri: resolveInfo) {
            names.add(ri.activityInfo.packageName);
            Logger.getLogger().d(ri.activityInfo.packageName);
        }
        return names;
    }

    /**
     * 判断当前应用是否是home
     * @param context
     * @return
     */
    public static boolean isHome() {
        String packageName = Application.getInstance().getPackageName();
        List<String> homePackageNames = getHomes();
        if (Toolkit.isEmpty(homePackageNames)) {
            return false;
        } else {
            return homePackageNames.contains(packageName);
        }
    }

    /**
     * 判断当前界面是否是桌面
     */
    public static boolean isInHome() {
        ActivityManager mActivityManager = (ActivityManager) Application.getInstance()
                .getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningTaskInfo> rti = mActivityManager.getRunningTasks(1);
        List<String> homePackageNames = getHomes();
        if (Toolkit.isEmpty(homePackageNames)) {
            return false;
        } else {
            return homePackageNames.contains(rti.get(0).topActivity.getPackageName());
        }
    }

    /**
     * 保存当前版本号
     * @return
     */
    public static void saveCurrVersion(Context context) {
        String currVersion = Toolkit.getLocalVersion(context);
        Logger.getLogger().d("save currVersion to SharedPreferences, currVersion:" + currVersion);
        SharedPreferences mPreference = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor mEditor = mPreference.edit();
        mEditor.putString(Constants.CURRVERSION, currVersion);
        mEditor.commit();
    }

    /**
     * android图片的内存优化
     */
    public static void setBackgroundResource(Context context, View view, int resId) {
        Bitmap bm = readBitMap(context, resId);
        BitmapDrawable bd = new BitmapDrawable(context.getResources(), bm);
        view.setBackgroundDrawable(bd);
    }

    /**
     * 及时释放内存
     */
    public static void recycle(Context context, View view) {
        BitmapDrawable bd = (BitmapDrawable) view.getBackground();
        if (bd != null) {
            bd.setCallback(null);
            Bitmap bmp = bd.getBitmap();
            bmp.recycle(); //回收图片所占的内存        
        }
    }

    /**
     * 以最省内存的方式读取本地资源的图片
     * @param context
     * @param resId
     * @return
     */
    public static Bitmap readBitMap(Context context, int resId) {
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inPreferredConfig = Bitmap.Config.RGB_565;
        opt.inPurgeable = true;
        opt.inInputShareable = true;
        //获取资源图片     
        InputStream is = context.getResources().openRawResource(resId);
        return BitmapFactory.decodeStream(is, null, opt);
    }

    public static void openStrictMode() {
        if (Toolkit.isDebuggable() && VERSION.SDK_INT >= 9) {
            Logger.getLogger().e("Open strict mode!");
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll()
                    .penaltyLog().build());
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().penaltyLog()
                    .build());
        }
    }

}
